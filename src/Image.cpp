#include <iostream>
#include <fstream>
using namespace std;

#include "Image.h"
#include "Pixel.h"





Image::Image()
{
    dimx=0;
    dimy=0;
}


Image::Image(unsigned int x, unsigned int y)
{
    dimx=x;
    dimy=y;
    tab.resize(dimx * dimy);
}


Image::~Image() 
{
    tab.clear();
}

Pixel& Image::getPix (int x,int y) 
{
    if (x>=0 && x<=dimx && y>=0 && y<=dimy)
    {
        return tab[y*dimx+x]; 
    }
}

Pixel  Image::getPix(int x, int y) const
{
    if (x>=0 && x<=dimx && y>=0 && y<=dimy)
    {
        return tab[y*dimx+x]; 
    }
}

void Image::setPix (int x, int y, Pixel couleur)
{
    if (x>=0 && x<=dimx && y>=0 && y<=dimy)
    {
        tab[y*dimx+x]=couleur;
    }
}

void Image::effacer(Pixel couleur)
{
    dessinerRectangle(0,0,dimx-1,dimy-1,couleur);
}

/*void Image::testRegression()
{
    getPix(2,3)
}*/

void Image::dessinerRectangle(int Xmin,int Ymin,int Xmax,int Ymax,Pixel couleur)
{
    for(int i=Xmin;i<=Xmax;i++)
    {
        for(int j=Ymin;j<=Ymax;j++)
        {
            setPix(i,j,couleur);
        }
    }
}

void Image::sauver(const string &filename) const
{
    ofstream fichier(filename.c_str());
    if (!fichier.is_open())
    {
        cout << "Erreur à l'ouverture de l'image"<<endl;
    }
    for (int y = 0; y < dimy; y++)
    {
        for (int x = 0; x < dimx; x++)
        {
            Pixel p = getPix(x, y);
            fichier << int(p.r) << endl; 
            fichier << int(p.g) << endl;
            fichier << int(p.b) << endl;
        }
    }
    cout << "Votre Image est Sauvegardé";
    fichier.close();
}

void Image::ouvrir(const string &filename)
{
    ifstream fichier(filename.c_str());
    if (!fichier.is_open())
    {
        cout << "Erreur à l'ouverture de votre fichier" << endl;
    }
    char r, g, b;
    string mot;
    dimx = 0;
    dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    if (dimx <= 0 && dimy <= 0);
    {
        cout << "Erreur" <<endl;
        return;
    }
    if (!tab.empty())
        {
            tab.clear();
        }
    tab.resize(dimx*dimy);
    for (int y = 0; y < dimy; ++y)
    {
        for (int x = 0; x < dimx; ++x)
        {
            fichier >> r >> g >> b;
            getPix(x, y).r = r;
            getPix(x, y).g = g;
            getPix(x, y).b = b;
        }
    }   
    fichier.close();
}

void Image::afficherConsole()
{
    for (int y = 0; y < dimy; y++)
    {
        for (int x = 0; x < dimx; x++)
        {
            Pixel p = getPix(x, y);
            cout << int(p.r) << endl; 
            cout << int(p.g) << endl;
            cout << int(p.b) << endl;
        }
    }
}

