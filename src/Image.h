#ifndef _IMAGE_H
#define _IMAGE_H


#include <string>
#include <vector>

#include "Pixel.h"


using namespace std;

class Image
{
    private:
        int dimx,dimy;
        vector<Pixel> tab;
    



    public:
    

    Image();


    Image (unsigned int x, unsigned  int y);


    ~Image ();


    bool isValid(int x, int y)const;


    static void testRegression();


    Pixel&  getPix (int x,int y);


    Pixel  getPix(int x, int y) const;


    void setPix (int x, int y, Pixel couleur);


    void dessinerRectangle(int Xmin,int Ymin,int Xmax,int Ymax,Pixel couleur);


    void effacer(Pixel couleur);


    void sauver(const string &filename)const;


    void ouvrir(const string &filename);


    void afficherConsole();


};


#endif
