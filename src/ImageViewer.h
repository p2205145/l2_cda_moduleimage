#ifndef _IMAGEVIEWER_H
#define _IMAGEVIEWER_H

#include <SDL2/SDL.h>

#include "Image.h"

class ImageViewer{

    private:
        SDL_Window * f;
        SDL_Renderer * r;
        SDL_Surface * m_surface;
        SDL_Texture * m_texture;
    public:
        ImageViewer();
        ~ImageViewer();
        void afficher(Image im);

};

#endif
