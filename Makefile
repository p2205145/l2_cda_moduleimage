all: bin/test bin/exemple bin/affichage

bin/test: obj/mainTest.o obj/Image.o
		g++ -g obj/mainTest.o obj/Image.o -o bin/test

obj/mainTest.o: src/mainTest.cpp src/Pixel.h src/Image.h
		g++ -g -Wall -c src/mainTest.cpp -o obj/mainTest.o

obj/Image.o: src/Pixel.h src/Image.h src/Image.cpp
		g++ -g -Wall -c src/Image.cpp -o obj/Image.o

bin/exemple: obj/mainExemple.o obj/Image.o
		g++ -g obj/mainExemple.o obj/Image.o -o bin/exemple

obj/mainExemple.o: src/mainExemple.cpp src/Image.h
		g++ -g -Wall -c src/mainExemple.cpp -o obj/mainExemple.o

bin/affichage: obj/mainAffichage.o
		mkdir -p bin ; g++ -o bin/affichage obj/mainAffichage.o obj/Image.o

obj/mainAffichage.o: src/mainAffichage.cpp src/ImageViewer.h src/Image.h
		g++ -g -Wall -c src/mainAffichage.cpp -o obj/mainAffichage.o

obj/ImageViewer.o: src/ImageViewer.h src/ImageViewer.cpp src/Image.h
		g++ -g -Wall -c src/ImageViewer.cpp -o obj/ImageViewer.o

doc: doc/doxyfile
	doxygen doc/doxyfile

clean:
		rm obj/*.o

veryclean: clean
		rm *.out